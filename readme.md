# The Artist    

 TheArtist est un site web permettant de visualiser les différentes peintures de ce dernier

 ## Environnement de développement

 ### Pré-requis

 * PHP 7.4
 * Composer
 * Symfony CLI
 * Docker
 * Docker-compose

 Vous pouvez vérifier les pré-requis (Sauf Docker & Docker-compose) via la commande suivante :
 
 ``` bash
 symfony check:requirements
 ```

 ### Lancer l'environnement de développement 
 ``` bash
 docker-composer up -d
 symfony serve -d
 ```